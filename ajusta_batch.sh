#!/bin/bash

### Criado por Frederico Augusto Santos Brasil
###
###
###

ARQORIGINAL=$1;
SIGLA=$2;
HOJE=$(date +'%Y%m%d');
HOJE2=$(date +'%d%m%Y');

echo "apagar todas as linhas vazias";
sed '/^$/d' $ARQORIGINAL > TMP1.TXT;

echo "apagar todos os espaços em branco";
tr -d ' ' < TMP1.TXT > TMP2.TXT;

echo "todas em maiusculo";
sed 'y/abcdefghijklmnopqrstuvwxyzàáâãéêíóôõúç/ABCDEFGHIJKLMNOPQRSTUVWXYZÀÁÂÃÉÊÍÓÔÕÚÇ/' TMP2.TXT > TMP3.TXT;

echo "apagar todas as linhas que não comecem por 1I";
sed -n '/1I/p' TMP3.TXT > TMP4.TXT;

echo "contar linhas";
LINHAS=$(printf %07d "$(wc -l < TMP4.TXT)");

echo "inserir linha com string 026240CADADICIONAIS$HOJE";
sed -e '1i\' -e '026240CADADICIONAIS'$HOJE2 TMP4.TXT > TMP5.TXT;

echo "Incluir linha após a última";
sed -e '$a\' -e '9'$LINHAS TMP5.TXT > TMP6.TXT

echo "Nome do arquivo formatado para envio: $HOJE-$SIGLA-FORMATADO.txt";

cp TMP6.TXT $HOJE-$SIGLA-FORMATADO.txt;
